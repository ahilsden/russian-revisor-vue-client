import { mount } from '@vue/test-utils'
import axios from "axios";
import flushPromises from "flush-promises"
import CreateNoun from "@/components/CreateNoun";
import App from "@/App";

// https://next.vue-test-utils.vuejs.org/guide/advanced/async-suspense.html

const mockNoun = [
    {
        "id": 1,
        "english": "English word 1",
        "russian": "Russian word 1",
        "gender": "f",
        "notes": "Some notes"
    }
]

jest.mock("axios", () => ({
    get: () => Promise.resolve({data: [
        {
            "id": 1,
            "english": "English word 1",
            "russian": "Russian word 1",
            "gender": "f",
            "notes": "Some notes"
        },
        {
            "id": 2,
            "english": "English word 2",
            "russian": "Russian word 2",
            "gender": "f",
            "notes": "Interesting word"
        }
    ]})
}))

describe('App.vue http requests',() => {
    it("should return nouns on instantiation", async () => {
        const wrapper = mount(App);
        const expectedNumberOfNouns = 2

        await flushPromises();
        expect(wrapper.vm.nouns.length).toBe(expectedNumberOfNouns);
    });
})

