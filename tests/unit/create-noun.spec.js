import { mount } from '@vue/test-utils'
import CreateNoun from "@/components/CreateNoun";
import flushPromises from "flush-promises";
import axios from "axios";

jest.mock("axios", () => ({
    post: jest.fn(() => Promise.resolve(true))
}))

// todo: test not working
describe('CreateNoun.vue', () => {
    describe('When the user clicks the submit button', () => {
        it('should create a noun', async () => {
            const wrapper = mount(CreateNoun);

            await wrapper.get('button').trigger('click')

            await flushPromises();

            expect(axios.post).toHaveBeenCalledTimes(1)
        })
    })
})
